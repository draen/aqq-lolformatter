object frmSettings: TfrmSettings
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Ustawienia statusu'
  ClientHeight = 286
  ClientWidth = 241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object edit: TMemo
    Left = 0
    Top = 0
    Width = 241
    Height = 249
    TabOrder = 0
  end
  object InputButton: TButton
    Left = 0
    Top = 255
    Width = 75
    Height = 25
    Caption = 'Wstaw'
    PopupMenu = popup
    TabOrder = 1
    OnClick = InputButtonClick
  end
  object OkButton: TButton
    Left = 166
    Top = 255
    Width = 75
    Height = 25
    Caption = 'Akceptuj'
    TabOrder = 2
    OnClick = OkButtonClick
  end
  object DefaultButton: TButton
    Left = 81
    Top = 255
    Width = 79
    Height = 25
    Caption = 'Domy'#347'lne'
    TabOrder = 3
    OnClick = DefaultButtonClick
  end
  object sSkinManager: TsSkinManager
    ExtendedBorders = True
    InternalSkins = <>
    MenuSupport.IcoLineSkin = 'ICOLINE'
    MenuSupport.ExtraLineFont.Charset = DEFAULT_CHARSET
    MenuSupport.ExtraLineFont.Color = clWindowText
    MenuSupport.ExtraLineFont.Height = -11
    MenuSupport.ExtraLineFont.Name = 'Tahoma'
    MenuSupport.ExtraLineFont.Style = []
    SkinDirectory = 'c:\Skins'
    SkinInfo = 'N/A'
    ThirdParty.ThirdEdits = 
      'TEdit'#13#10'TMemo'#13#10'TMaskEdit'#13#10'TLabeledEdit'#13#10'THotKey'#13#10'TListBox'#13#10'TCheck' +
      'ListBox'#13#10'TRichEdit'#13#10'TDateTimePicker'
    ThirdParty.ThirdButtons = 'TButton'
    ThirdParty.ThirdBitBtns = 'TBitBtn'
    ThirdParty.ThirdCheckBoxes = 'TCheckBox'#13#10'TRadioButton'#13#10'TGroupButton'
    ThirdParty.ThirdGroupBoxes = 'TGroupBox'#13#10'TRadioGroup'
    ThirdParty.ThirdListViews = 'TListView'
    ThirdParty.ThirdPanels = 'TPanel'
    ThirdParty.ThirdGrids = 'TStringGrid'#13#10'TDrawGrid'
    ThirdParty.ThirdTreeViews = 'TTreeView'
    ThirdParty.ThirdComboBoxes = 'TComboBox'#13#10'TColorBox'
    ThirdParty.ThirdWWEdits = ' '
    ThirdParty.ThirdVirtualTrees = ' '
    ThirdParty.ThirdGridEh = ' '
    ThirdParty.ThirdPageControl = 'TPageControl'
    ThirdParty.ThirdTabControl = 'TTabControl'
    ThirdParty.ThirdToolBar = 'TToolBar'
    ThirdParty.ThirdStatusBar = 'TStatusBar'
    ThirdParty.ThirdSpeedButton = 'TSpeedButton'
    ThirdParty.ThirdScrollControl = 'TScrollBox'
    ThirdParty.ThirdUpDown = 'TUpDown'
    ThirdParty.ThirdScrollBar = 'TScrollBar'
    ThirdParty.ThirdStaticText = 'TStaticText'
    Left = 200
    Top = 48
  end
  object sSkinProvider: TsSkinProvider
    AddedTitle.Font.Charset = DEFAULT_CHARSET
    AddedTitle.Font.Color = clNone
    AddedTitle.Font.Height = -11
    AddedTitle.Font.Name = 'Tahoma'
    AddedTitle.Font.Style = []
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 200
    Top = 8
  end
  object popup: TPopupMenu
    Left = 200
    Top = 96
    object Poziom1: TMenuItem
      Caption = 'Poziom'
      OnClick = Poziom1Click
    end
    object Champion1: TMenuItem
      Caption = 'Champion'
      OnClick = Champion1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Wygrane1: TMenuItem
      Caption = 'Wygrane'
      OnClick = Wygrane1Click
    end
    object Wyjcia1: TMenuItem
      Caption = 'Wyj'#347'cia'
      OnClick = Wyjcia1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object RankedWygrane1: TMenuItem
      Caption = 'Ranked - Wygrane'
      OnClick = RankedWygrane1Click
    end
    object RankedPrzegrane1: TMenuItem
      Caption = 'Ranked - Przegrane'
      OnClick = RankedPrzegrane1Click
    end
    object RankedRating1: TMenuItem
      Caption = 'Ranked - Rating'
      OnClick = RankedRating1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Stangry1: TMenuItem
      Caption = 'Stan gry'
      OnClick = Stangry1Click
    end
    object Opis1: TMenuItem
      Caption = 'Opis'
      OnClick = Opis1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Dywizja1: TMenuItem
      Caption = 'Dywizja'
      OnClick = Dywizja1Click
    end
    object ier1: TMenuItem
      Caption = 'Tier'
      OnClick = ier1Click
    end
  end
end
