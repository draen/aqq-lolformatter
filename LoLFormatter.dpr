// Copyright (C) 2014 Rafa� Szczerbi�ski
//
// This file is part of LoLFormatter
//
// LoLFormatter is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3, or (at your option)
// any later version.
//
// LoLFormatter is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GNU Radio; see the file COPYING. If not, write to
// the Free Software Foundation, Inc., 51 Franklin Street,
// Boston, MA 02110-1301, USA.

library LoLFormatter;

uses
  Windows,
  SysUtils,
  PluginAPI,
  Classes,
  INIFiles,
  frmSettingsUnit in 'frmSettingsUnit.pas' {frmSettings};

type
  Thread = class(TThread)
  protected
    procedure Execute; override;
  end;

var
  PluginInfo: TPluginInfo;
  PluginLink: TPluginLink;
  PluginContact: TPluginContact;

{$R *.res}

function GetThemeSkinDir: UnicodeString; stdcall;
begin
  Result := PWideChar(PluginLink.CallService(AQQ_FUNCTION_GETTHEMEDIR, 0, 0)) + '\skin';
end;

function GetPluginDir: UnicodeString; stdcall;
begin
  Result := PWideChar(PluginLink.CallService(AQQ_FUNCTION_GETPLUGINUSERDIR, 0, 0));
end;

function GetBorderColorHue: Integer; stdcall;
begin
  Result := PluginLink.CallService(AQQ_SYSTEM_COLORGETHUE, 0, 0);
end;

function GetBorderColorSaturation: Integer; stdcall;
begin
  Result := PluginLink.CallService(AQQ_SYSTEM_COLORGETSATURATION, 0, 0);
end;

function ChkSkinEnabled: Boolean; stdcall; { by BEHERIT }
var
  SettingsIni: TStringList;
  SettingsMem: TMemINIFile;
  Value: Boolean;
begin
  settingsIni := TStringList.Create;
  settingsIni.SetText(PWideChar(PluginLink.CallService(AQQ_FUNCTION_FETCHSETUP, 0, 0)));
  SettingsMem := TMemIniFile.Create(GetPluginDir + 'settings.ini');
  SettingsMem.SetStrings(SettingsIni);
  SettingsIni.Free;
  Value := SettingsMem.ReadBool('Settings', 'UseSkin', true);
  SettingsMem.Free;
  Result := Value;
end;

//Hooks
function OnModulesLoad(wParam: WPARAM; lParam: LPARAM): Integer; stdcall; forward;
function OnSetHTMLStatus(wParam: WPARAM; lParam: LPARAM): Integer; stdcall; forward;

function AQQPluginInfo(AQQVersion: DWord): PPluginInfo; stdcall;
begin
  PluginInfo.cbSize := SizeOf(TPluginInfo);
  PluginInfo.ShortName := 'LoL Formatter';
  PluginInfo.Version := PLUGIN_MAKE_VERSION(1,0,0,7);
  PluginInfo.Description := 'Formatowanie opis�w kontakt�w LoL';
  PluginInfo.Author := 'Rafa� Szczerbi�ski (Draen)';
  PluginInfo.AuthorMail := 'draen94@gmail.com';
  PluginInfo.Copyright := 'Draen';
  PluginInfo.Homepage := 'http://draen.y0.pl';
  PluginInfo.Flag := 0;
  PluginInfo.ReplaceDefaultModule := 0;

  Result := @PluginInfo;
end;

function OnModulesLoad(wParam: WPARAM; lParam: LPARAM): Integer; stdcall;
begin
  Result := 0;
end;

function GetString(source, param: UnicodeString): UnicodeString;
begin
  param := '<'+param+'>';
  if Pos(param, source) > 0 then
  begin
    Delete(source, 1, Pos(param, source)+Length(param)-1);
    Delete(source, Pos('<', source), Length(source));
    Result := source;
  end
  else Result := '';
end;

procedure Thread.Execute;
var
  TempContact: TPluginContact;
begin
  FreeOnTerminate := True;
  TempContact := PluginContact;
  if AnsiCompareStr(GetString(TempContact.Status, 'gameStatus'), 'inGame') = 0 then
    PluginLink.CallService(AQQ_CONTACTS_SETWEB_AVATAR, UINT_PTR('http://how2play.pl/img/'+GetString(TempContact.Status, 'skinname')+'_Square_0.png'), INT_PTR(@TempContact))
  else
    PluginLink.CallService(AQQ_CONTACTS_SETWEB_AVATAR, UINT_PTR('http://draen.y0.pl/img/lol/profileIcon'+GetString(TempContact.Status, 'profileIcon')+'.jpg'), INT_PTR(@TempContact));
end;

function OnSetHTMLStatus(wParam: WPARAM; lParam: LPARAM): Integer; stdcall;
var
  Body: UnicodeString;
  F: TextFile;
  S: UnicodeString;
  old: UnicodeString;
begin
  if (Pos('@pvp.net', PPluginContact(wParam)^.JID) <> 0) AND (Length(UnicodeString(lParam)) > 0) then
  begin
    PluginContact := PPluginContact(wParam)^;
    Thread.Create(False);

    AssignFile(F, GetPluginDir + '\League\status');
    Reset(F);
    Body := '';
    while not EOF(F) do
    begin
      Readln(F, S);
      if (S <> '</br>') AND (S <> '<br>') then Body := Body + S + '</br>'
      else Body := Body + S;
    end;
    CloseFile(F);
    while Pos('[', Body) > 0 do
    begin
      old := Copy(Body, Pos('[', Body)+1, Pos(']', Body) - Pos('[', Body)-1);
      Body := StringReplace(Body, '['+old+']', GetString(PPluginContact(wParam)^.Status, old), []);
    end;

    Result := INT_PTR(Body);
  end
  else Result := 0;
end;

function OnReceiveMessage(wParam: WPARAM; lParam: LPARAM): Integer; stdcall;
var
  Body: UnicodeString;
  old: UnicodeString;
  mapName: UnicodeString;
  gameType: UnicodeString;
begin
  if (Pos('@pvp.net', PPluginContact(wParam)^.JID) <> 0) AND (Pos('<inviteId>', PPluginMessage(lParam)^.Body) <> 0) then
  begin
    Result := 2;
    old := PPluginMessage(lParam)^.Body;
    case StrToInt(GetString(old, 'mapId')) of
      1: mapName := 'Summoner''s Rift';
      2: mapName := 'Summoner''s Rift';
      3: mapName := 'The Proving Grounds';
      4: mapName := 'Twisted Treeline';
      8: mapName := 'The Crystal Scar';
      10: mapName := 'Twisted Treeline';
      12: mapName := 'Howling Abyss'
    end;
    gameType := GetString(old, 'gameType');
    if gameType = 'PRACTICE_GAME' then gameType := ' niestandardowej'
    else if gameType = 'COOP_VS_AI' then gameType := ' przeciwko SI'
    else if gameType = 'RANKED_GAME_PREMADE' then gameType := ' rankingowej';


    Body := '<< Przywo�ywacz ' + GetString(old, 'userName') + ' zaprasza Ci� do gry' +
      gameType + ' na mapie ' + mapName + '. >>';
    PPluginMessage(lParam)^.Body := PWideChar(Body);
  end
  else Result := 0;
end;

function Settings: Integer; stdcall;
begin
  Result := 0;
  frmSettings := TfrmSettings.Create(nil);
  frmSettings.Show;
end;

function Load(Link: PPluginLink): Integer; stdcall;
begin
  PluginLink := Link^;
  PluginLink.HookEvent(AQQ_SYSTEM_MODULESLOADED, OnModulesLoad);
  PluginLink.HookEvent(AQQ_CONTACTS_SETHTMLSTATUS, OnSetHTMLStatus);
  PluginLink.HookEvent(AQQ_CONTACTS_RECVMSG, OnReceiveMessage);
  Result := 0;
end;

function Unload: Integer; stdcall;
begin
  Result := 0;
  PluginLink.UnhookEvent(THandle(@OnModulesLoad));
  PluginLink.UnhookEvent(THandle(@OnSetHTMLStatus));
  PluginLink.UnhookEvent(THandle(@OnReceiveMessage));
end;

exports
  Load,
  Unload,
  AQQPluginInfo,
  Settings,
  GetThemeSkinDir name 'GetThemeSkinDir',
  GetPluginDir name 'GetPluginDir',
  GetBorderColorHue name 'GetBorderColorHue',
  GetBorderColorSaturation name 'GetBorderColorSaturation',
  ChkSkinEnabled name 'GetAdvancedSkinState';

begin
end.