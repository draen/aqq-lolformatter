// Copyright (C) 2014 Rafa� Szczerbi�ski
//
// This file is part of LoLFormatter
//
// LoLFormatter is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3, or (at your option)
// any later version.
//
// LoLFormatter is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GNU Radio; see the file COPYING. If not, write to
// the Free Software Foundation, Inc., 51 Franklin Street,
// Boston, MA 02110-1301, USA.

unit frmSettingsUnit;

interface

uses
  Windows, Forms, Classes, sSkinProvider, Messages, Vcl.Imaging.PNGImage,
  sSkinManager, SysUtils, Vcl.Controls, Vcl.StdCtrls, Vcl.Menus;

type
  TGetPluginDir = function: UnicodeString; stdcall;
  TGetThemeSkinDir = function: UnicodeString; stdcall;
  TGetBorderColorHue = function: Integer; stdcall;
  TGetBorderColorSaturation = function: Integer; stdcall;
  TGetAdvancedSkinState = function: Boolean; stdcall;
  TfrmSettings = class(TForm)
    sSkinManager: TsSkinManager;
    sSkinProvider: TsSkinProvider;
    edit: TMemo;
    InputButton: TButton;
    OkButton: TButton;
    DefaultButton: TButton;
    popup: TPopupMenu;
    Poziom1: TMenuItem;
    Champion1: TMenuItem;
    N1: TMenuItem;
    Wygrane1: TMenuItem;
    Wyjcia1: TMenuItem;
    N2: TMenuItem;
    RankedWygrane1: TMenuItem;
    RankedPrzegrane1: TMenuItem;
    RankedRating1: TMenuItem;
    N3: TMenuItem;
    Stangry1: TMenuItem;
    Opis1: TMenuItem;
    N4: TMenuItem;
    Dywizja1: TMenuItem;
    ier1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure InputButtonClick(Sender: TObject);
    procedure DefaultButtonClick(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
    procedure Poziom1Click(Sender: TObject);
    procedure Champion1Click(Sender: TObject);
    procedure Wygrane1Click(Sender: TObject);
    procedure Wyjcia1Click(Sender: TObject);
    procedure RankedWygrane1Click(Sender: TObject);
    procedure RankedPrzegrane1Click(Sender: TObject);
    procedure RankedRating1Click(Sender: TObject);
    procedure Stangry1Click(Sender: TObject);
    procedure Opis1Click(Sender: TObject);
    procedure Dywizja1Click(Sender: TObject);
    procedure ier1Click(Sender: TObject);
  private
    procedure AlphaWindowsCompatibility(var msg: TMessage); message WM_USER+666;
  public
  end;

var
  frmSettings: TfrmSettings;
  PluginDir: UnicodeString;
  PreviewEnabled: Boolean = False;

implementation

{$R *.dfm}

procedure TfrmSettings.AlphaWindowsCompatibility(var msg: TMessage);
begin
  if sSkinManager.Active then sSkinProvider.BorderForm.UpdateExBordersPos(true, msg.LParam);
end;

procedure TfrmSettings.DefaultButtonClick(Sender: TObject);
begin
  edit.Lines.Clear;
  edit.Lines.Add('Poziom: [level]');
  edit.Lines.Add('</br>');
  edit.Lines.Add('<b>NORMAL</b> Wygrane: [wins], Wyj�cia: [leaves]');
  edit.Lines.Add('<b>RANKED</b> Wygrane: [rankedWins], Przegrane: [rankedLosses]');
  edit.Lines.Add('</br>');
  edit.Lines.Add('OPIS: [statusMsg]');
end;

procedure TfrmSettings.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmSettings.FormCreate(Sender: TObject);
var
  ThemeSkinDir: UnicodeString;
  DLL: THandle;
  GetPluginDir: TGetPluginDir;
  GetThemeSkinDir: TGetThemeSkinDir;
  GetBorderColorHue: TGetBorderColorHue;
  GetBorderColorSaturation: TGetBorderColorSaturation;
  GetAdvancedSkinState: TGetAdvancedSkinState;
begin
  DLL := LoadLibrary('LoLFormatter.dll');
  try
    @GetPluginDir := GetProcAddress(DLL, 'GetPluginDir');
    @GetBorderColorHue := GetProcAddress(DLL, 'GetBorderColorHue');
    @GetBorderColorSaturation := GetProcAddress(DLL, 'GetBorderColorSaturation');
    @GetAdvancedSkinState := GetProcAddress(DLL, 'GetAdvancedSkinState');
    @GetThemeSkinDir := GetProcAddress(DLL, 'GetThemeSkinDir');
    PluginDir := GetPluginDir;
    ThemeSkinDir := GetThemeSkinDir;
    if GetAdvancedSkinState then
    begin
      if FileExists(ThemeSkinDir + '\Skin.asz') then
      begin
        sSkinManager.SkinDirectory := ThemeSkinDir;
        sSkinManager.SkinName := 'Skin.asz';
        sSkinManager.HueOffset := GetBorderColorHue;
        sSkinManager.Saturation := GetBorderColorSaturation;
        sSkinManager.Active := true;
      end
      else sSkinManager.Active := false;
    end
    else sSkinManager.Active := false;
    if FileExists(PluginDir + '\League\status') then
      edit.Lines.LoadFromFile(GetPluginDir + '\League\status')
    else DefaultButton.Click;
  finally
    FreeLibrary(DLL);
  end;
end;

procedure TfrmSettings.InputButtonClick(Sender: TObject);
begin
  popup.Popup(mouse.CursorPos.X, mouse.CursorPos.Y);
end;

procedure TfrmSettings.OkButtonClick(Sender: TObject);
begin
  edit.Lines.SaveToFile(PluginDir + '\League\status');
  Close;
end;

procedure TfrmSettings.Champion1Click(Sender: TObject);
begin
  edit.SelText := '[skinname]';
end;

procedure TfrmSettings.ier1Click(Sender: TObject);
begin
  edit.SelText := '[rankedLeagueTier]';
end;

procedure TfrmSettings.Dywizja1Click(Sender: TObject);
begin
  edit.SelText := '[rankedLeagueDivision]';
end;

procedure TfrmSettings.Opis1Click(Sender: TObject);
begin
  edit.SelText := '[statusMsg]';
end;

procedure TfrmSettings.Poziom1Click(Sender: TObject);
begin
  edit.SelText := '[level]';
end;

procedure TfrmSettings.RankedPrzegrane1Click(Sender: TObject);
begin
  edit.SelText := '[ranekdLosses]';
end;

procedure TfrmSettings.RankedRating1Click(Sender: TObject);
begin
  edit.SelText := '[rankedRating]';
end;

procedure TfrmSettings.RankedWygrane1Click(Sender: TObject);
begin
  edit.SelText := '[rankedWins]';
end;

procedure TfrmSettings.Stangry1Click(Sender: TObject);
begin
  edit.SelText := '[gameStatus]';
end;

procedure TfrmSettings.Wygrane1Click(Sender: TObject);
begin
  edit.SelText := '[wins]';
end;

procedure TfrmSettings.Wyjcia1Click(Sender: TObject);
begin
  edit.SelText := '[leaves]';
end;

end.
